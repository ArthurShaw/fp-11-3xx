module SemesterWork where

import Data.Char

data Term = Lambda String Term
          | Apply Term Term
          | Var String
          deriving Eq

instance Show Term where
    show (Var v) = v
    show (Lambda v t) = "(\\" ++ v ++ "." ++ show t ++ ")"
    show (Apply t1 t2) = show t1 ++ " " ++ show t2

-- Редукция на 1 шаг
eval1 :: Term -> Term
eval1 (Apply term1@(Apply _ _) term2) = Apply (eval1 term1) term2
eval1 (Apply term1 term2@(Apply _ _)) = Apply term1 (eval1 term2)
eval1 (Apply (Lambda var body) term2) = replaceVariable var term2 body
eval1 term = term

-- Многошаговая редукция
eval :: Term -> Term
eval (Apply term1@(Apply (Var _) _) term2) = Apply (eval term1) term2
eval (Apply term1@(Apply _ _) term2) = eval $ Apply (eval term1) term2
eval (Apply (Lambda var body) term2) = eval $ replaceVariable var term2 body
eval (Apply term1 term2@(Apply (Var _) _)) = Apply term1 (eval term2)
eval (Apply term1 term2@(Apply _ _)) = eval $ Apply term1 (eval term2)
eval term = term

-- Заменить свободные вхождения var на term в body
replaceVariable :: String -> Term -> Term -> Term
replaceVariable var term body = case body of
    Var v -> if v == var then term else body
    Apply t1 t2 -> Apply (replaceVariable var replaced t1) (replaceVariable var replaced t2)
    lam@(Lambda v1 (Var v2)) -> if v1 /= v2 && v2 == var
        then Lambda v1 (replaced)
        else lam
    lam@(Lambda v1 lam2@(Lambda _ _)) -> if v1 == var
        then lam
        else Lambda v1 (replaceVariable var replaced lam2)
    lam@(Lambda v1 app2@(Apply _ _)) -> if v1 == var
        then lam
        else Lambda v1 (replaceVariable var replaced app2)
    where variables = getUsedVariables body
          replaced = case term of
            Var v -> Var (createNewName variables v)
            term -> term

-- Сгенерировать имя переменной, которое не содержится в used
createNewName :: [String] -> String -> String
createNewName used var = if var `elem` used
    then createNewName used (createNextName var)
    else var

-- Получить следующее значение переменной по порядку
createNextName :: String -> String
createNextName [ch] = if ch == 'z' then createNextName ['a', '`'] else [chr (ord ch + 1)]
createNextName (x:xs) = x:createNextName xs

-- Получить связанные переменные терма
getUsedVariables :: Term -> [String]
getUsedVariables term = case term of
    Var var -> [var]
    Lambda var body -> var : getUsedVariables body
    Apply term1 term2 -> getUsedVariables term1 ++ getUsedVariables term2